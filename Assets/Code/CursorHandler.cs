﻿using UnityEngine;
using System.Collections;

public class CursorHandler : MonoBehaviour
{
	public Texture2D cursorTexture;
	public CursorMode cursorMode = CursorMode.Auto;
    public bool cursortarget = false;
    public bool nocursor     = false;

	void Start()
	{
        if (cursortarget)
        {
            Cursor.SetCursor(cursorTexture, Vector2.zero, cursorMode);
        }
        if (nocursor)
        {
            Cursor.visible = false;
        }
    }
}