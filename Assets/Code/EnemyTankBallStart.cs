﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTankBallStart : MonoBehaviour
{
    public Transform explosion_start;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            Destroy(gameObject);
            var explosionstart = (Transform)Instantiate(explosion_start, transform.position, transform.rotation);
            Destroy(explosionstart.gameObject, 2);
        }
        if (collision.gameObject.name == "tankopen")
        {
            Destroy(gameObject);
            var explosionstart = (Transform)Instantiate(explosion_start, transform.position, transform.rotation);
            Destroy(explosionstart.gameObject, 2);
        }


    }
}
