﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCanonAttack : MonoBehaviour 
{
    public GameObject bullitPrefab;
    public GameObject bullitPrefab2;
    public Transform tankballspawn;
    public AudioClip canonfire1;
    AudioSource audioSource;
    public  bool xronos  =false;
    public  bool shootOn = false;
    //public float time=0.0f;
    //public float timemax = 0.0f;

    // public GameObject TrajectoryPointPrefeb;
    // private int numOfTrajectoryPoints = 30;
    // private List<Transform> trajectoryPoints = new List<Transform>();

    public void Start () 
	{
		audioSource = GetComponent<AudioSource>();
    }
	
	public void Update ()
	{
        //if ( (Input.GetKeyDown (KeyCode.Space) || (Input.GetMouseButtonDown(0)) ) && Tank.GUI_OverloadBar.fillAmount > 0.1f && Tank.GUI_AmmoBar.fillAmount > 0.09f && !Gui_Menu.menu)
        if ((Input.GetKeyDown(KeyCode.Space) || (Input.GetMouseButtonDown(0))) && Tank.Overload > 0 && Tank.Ammo > 0 && !Gui_Menu.menu)
        {
			shootOn = true; //bool for Shoot
			Shoot ();     
        } 
		else
		{
			shootOn = false;
		}

        //Trajectory Projectile Path
        if ((Input.GetKeyDown(KeyCode.F)|| (Input.GetMouseButtonDown(1))) && !Gui_Menu.menu)
        {
            ShootTrajectory();
        }
    }

    public void FixedUpdate()
    {
        if (Tank.Overload < 10)
        {
            //StartCoroutine (waittime());
            //time += Time.time;
            //timemax = time+.05f;
            shootOn = false;
            Invoke("RefillOverload", 5);
        }
    }
    public void Shoot()
	{
		var bullit = (GameObject)Instantiate( bullitPrefab,tankballspawn.position,tankballspawn.rotation );
		//bullit.GetComponent<Rigidbody>().AddForce(transform.forward * 10000);
		bullit.GetComponent<Rigidbody>().velocity = bullit.transform.forward * 70;
		audioSource.PlayOneShot(canonfire1, 0.5F);
		Destroy( bullit, 6.0f );
	}
    public void ShootTrajectory()
    {
        var bullit2 = (GameObject)Instantiate(bullitPrefab2, tankballspawn.position, tankballspawn.rotation);
        //bullit.GetComponent<Rigidbody>().AddForce(transform.forward * 10000);
        bullit2.GetComponent<Rigidbody>().velocity = bullit2.transform.forward * 70;
        Destroy(bullit2, 6.0f);
    }
    void RefillOverload()
    {    
        // if (time >= timemax)     // 5 <= 9
        //{ 
        if (Tank.Overload < 100)
        {
            Tank.GUI_OverloadBar.fillAmount += 0.2f;
            Tank.Overload += 20;
            Tank.GUI_OverloadText.text = Tank.overload1 + Tank.Overload.ToString() + " %";
        }
        // }
        //time1 = 0 , time1
        
    }

}