﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCanonAttack2 : MonoBehaviour
{
    public GameObject bullitPrefab;
    public Transform enemytankballspawn;
    public AudioClip canonfire1;
    AudioSource audioSource;
    public Image Enemy_GUI_OverloadBar;
    public Transform LookAtTarget;
    float saveTime = 0;
    //public bool xronos2 = false;
    //public bool shootOn2 = false;
    int damp = 40;
    //public EnemyTank m_enemytank;

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();
        LookAtTarget = GameObject.Find("Tank").transform.Find("middletank").transform.Find("uppertank1");
    }
    public void Update()
    {
     float dist = Vector3.Distance(LookAtTarget.position, transform.position);

        if (dist<=50)
        {
            Attack(55);
        }
        else if (dist <= 80)
        {
            Attack(70);
        }
        else if (dist <=90)
        {
            Attack(80);
        }
        else
        {
            //var rotate2 = Quaternion.LookRotation(GameObject.Find(this.transform.parent.transform.parent.name).transform.position);
            //transform.localRotation = Quaternion.Slerp(transform.localRotation, rotate2, Time.deltaTime * 10);
        }
    }

    public void Attack(int force1)
    {
        var rotate = Quaternion.LookRotation(LookAtTarget.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotate, Time.deltaTime * damp);

        int seconds = (int)Time.time;
        //Debug.Log("seconds : "+seconds);
        int oddeven = (seconds % 2);
        //Debug.Log("oddeven : "+oddeven);

        if (oddeven == 1)
        {
            Shoot(seconds, force1);
        }
    }
    public int Shoot(int seconds,int force)
    {
        //float seconds2 = Time.time;
        if (seconds != saveTime)
        {
         var bullit = (GameObject)Instantiate(bullitPrefab, enemytankballspawn.position, enemytankballspawn.rotation);
         //bullit.GetComponent<Rigidbody>().AddForce(transform.forward * 10000);
         bullit.GetComponent<Rigidbody>().velocity = bullit.transform.forward * force;

         saveTime = seconds;
         audioSource.PlayOneShot(canonfire1, 0.5F);
         Destroy(bullit, 6.0f);
        }
        return seconds;
    }

    

}