﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jeep : MonoBehaviour
{
    public int caseJeepSwitch;
    public GameObject Waypoints;
    public Transform waypoint1;
    public Transform waypoint2;
    public Transform waypoint3;
    public Transform waypoint4;
    public Transform waypoint5;
    public Transform waypoint6;
    public Transform waypoint7;
    public Transform waypoint8;
    public Transform waypoint9;
    public Transform waypoint10;

    void Start()
    {
        caseJeepSwitch = Random.Range(1, 12);

        Waypoints  = GameObject.Find("EnemyWaypoints");
        waypoint1  = Waypoints.transform.Find("WP1");
        waypoint2  = Waypoints.transform.Find("WP2");
        waypoint3  = Waypoints.transform.Find("WP3");
        waypoint4  = Waypoints.transform.Find("WP4");
        waypoint5  = Waypoints.transform.Find("WP5");
        waypoint6  = Waypoints.transform.Find("WP6");
        waypoint7  = Waypoints.transform.Find("WP7");
        waypoint8  = Waypoints.transform.Find("WP8");
        waypoint9  = Waypoints.transform.Find("WP9");
        waypoint10 = Waypoints.transform.Find("WP10");
    }

    void Update()
    {

        switch (caseJeepSwitch)
        {
            case 1:
                Move(waypoint1.position);
                break;
            case 2:
                Move(waypoint2.position);
                break;
            case 3:
                Move(waypoint3.position);
                break;
            case 4:
                Move(waypoint4.position);
                break;
            case 5:
                Move(waypoint5.position);
                break;
            case 6:
                Move(waypoint6.position);
                break;
            case 7:
                Move(waypoint7.position);
                break;
            case 8:
                Move(waypoint8.position);
                break;
            case 9:
                Move(waypoint9.position);
                break;
            case 10:
                Move(waypoint10.position);
                break;
        }
    }

    public void Move(Vector3 positionnew)
    {
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, positionnew, 8 * Time.deltaTime);
        Quaternion newDir = Quaternion.LookRotation(positionnew - transform.position);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, newDir, Time.deltaTime * 10);
    }
}
