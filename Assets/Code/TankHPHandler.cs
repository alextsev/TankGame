﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankHPHandler : MonoBehaviour
{
    public Rigidbody tankrigidbody;

    private void Start()
    {
        tankrigidbody = GameObject.Find("Tank").GetComponent<Rigidbody>();
    }
    private void Update()
    {
    }
    void OnCollisionEnter(Collision Col)
    {
        if (Col.gameObject.tag == "EnemyTankBall")
        {
            tankrigidbody.angularVelocity = Vector3.zero;
            //Debug.Log("AFTER COL: " + tankrigidbody.angularVelocity);
            Tank.Hp -= 10;
            Tank.GUI_HpText.text = Tank.hp1 + Tank.Hp.ToString() + " %";
            Tank.GUI_HpBar.fillAmount -= 0.1f;
        }
        if (Col.gameObject.tag == "EnemyTank")
        {
            tankrigidbody.angularVelocity = Vector3.zero;
        }
    }
    void OnCollisionExit(Collision Col)
    {
    }
}
