﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankUpperTankMovementRot : MonoBehaviour 
{
	public Transform UpperTank;
	public Transform Canon;
	public Transform Tank;

    float rotationX;
    float minRotation = 9f;
    float maxRotation = 330f; //-30f
    //int turnSpeed = 5;

    private Vector3 mouse_moveInput;
    private Vector3 move_Velocity;

    void Start()
	{    
    }

	void Update () 
	{
            //UP && DOWN MOVEMENT
        if ((Input.GetKey( KeyCode.UpArrow) || (Input.GetAxis("Mouse ScrollWheel") > 0)) && !Gui_Menu.menu)
        {
            Canon.transform.Rotate(new Vector3(-0.95f, 0, 0));
            if ( Canon.transform.localEulerAngles.x < maxRotation && Canon.transform.localEulerAngles.x > minRotation)//dhladh rotationX >= -31 enw theloume >-30
            {
                Canon.transform.localEulerAngles = new Vector3(maxRotation,0,0);
                //Canon.transform.Rotate(Vector3.right, -turnSpeed * Time.deltaTime);
            }
        }
		else if ((Input.GetKey( KeyCode.DownArrow) || (Input.GetAxis("Mouse ScrollWheel") < 0)) && !Gui_Menu.menu)
        {
            Canon.transform.Rotate(new Vector3(0.95f, 0, 0));
            if (Canon.transform.localEulerAngles.x > minRotation && Canon.transform.localEulerAngles.x < maxRotation)//dhladh rotationX >= -31 enw theloume >-30
            {
                Canon.transform.localEulerAngles = new Vector3(minRotation, 0, 0);
                //Canon.transform.Rotate(Vector3.right, turnSpeed * Time.deltaTime);
            }     
        }
        //RIGHT && LEFT MOVEMENT
        if ((Input.GetKey(KeyCode.RightArrow) || (Input.GetAxis("Mouse X") > 0)) && !Gui_Menu.menu)
        {
            UpperTank.transform.localRotation = Quaternion.Euler(UpperTank.transform.localEulerAngles + new Vector3(0, 0.85f, 0));
        }
        else if ((Input.GetKey(KeyCode.LeftArrow)|| (Input.GetAxis("Mouse X") < 0)) && !Gui_Menu.menu)
        {
            UpperTank.transform.localRotation = Quaternion.Euler(UpperTank.transform.localEulerAngles + new Vector3(0, -0.85f, 0));
        }	
    }
}