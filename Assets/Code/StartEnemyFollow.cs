﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartEnemyFollow : MonoBehaviour
{
    public Transform LookAtTarget;


    public GameObject bullitPrefab;
    public Transform enemytankballspawn;
    public AudioClip canonfire1;
    AudioSource audioSource;
    float saveTime = 0;
    float startTime;
    float seconds_float;

    public void Start()
    {
        LookAtTarget = GameObject.Find("human@Running").transform;
        audioSource = GetComponent<AudioSource>();
        startTime = Time.time;
    }
    public void Update()
    {

        var rotate = Quaternion.LookRotation(LookAtTarget.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotate, Time.deltaTime * 8);

        seconds_float = Time.time-startTime;
        int seconds = (int)seconds_float;
        int oddeven = (seconds % 2);

        if (oddeven == 1 && seconds<6)
        {
            Shoot(seconds);
        }
    }
    public int Shoot(int seconds)
    {
        if (seconds != saveTime)
        {
            var bullit = (GameObject)Instantiate(bullitPrefab, enemytankballspawn.position, enemytankballspawn.rotation);
            bullit.GetComponent<Rigidbody>().velocity = bullit.transform.forward * 55;
            saveTime = seconds;
            audioSource.PlayOneShot(canonfire1, 0.5F);
            Destroy(bullit, 6.0f);
        }
        return seconds;
    }
}