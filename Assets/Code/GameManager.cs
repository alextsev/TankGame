﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	
	private Tank m_MainTank;

	private List<Tank> m_Tanks = new List<Tank>();

    //private EnemyTank EnemyTank;
    //private List<EnemyTank> m_EnemyTanks = new List<EnemyTank>();

    //public bool isFriendly;
    //private int[] enmTank = new int [10];
    public static Text LevelCompleted;

    public void Start ()
	{
        LevelCompleted = GameObject.Find("LevelCompleted").GetComponent<Text>();

        m_MainTank = new Tank ();
		m_MainTank.Start ("Tank");

		m_MainTank.MainObject.transform.position = new Vector3 (325, 0, 232);
		m_MainTank.MainObject.name = "Tank";

        LoadTanks("level1", 5);
        LoadTanks("level2", 7);
        LoadTanks("level3", 10);

    }
	// Update is called once per frame
	public void Update ()
	{
		m_MainTank.Update ();
    }

    private void FixedUpdate()
    {

        if (GameObject.Find("Enemy Tank 0") || GameObject.Find("Enemy Tank 1") || GameObject.Find("Enemy Tank 2") || GameObject.Find("Enemy Tank 3") || GameObject.Find("Enemy Tank 4") || GameObject.Find("Enemy Tank 5") || GameObject.Find("Enemy Tank 6") || GameObject.Find("Enemy Tank 7") || GameObject.Find("Enemy Tank 8") || GameObject.Find("Enemy Tank 9"))
        {
            LevelCompleted.enabled = false;
        }
        else
        {
            LevelCompleted.enabled = true;
            Time.timeScale = 1;
            Invoke("levelLoadDelay", 4);         
        }
    }

    public void levelLoadDelay()
    {
        if(SceneManager.GetActiveScene().name == "level1")
        {
            SceneManager.LoadScene("level2");
        }
        if (SceneManager.GetActiveScene().name == "level2")
        {
            SceneManager.LoadScene("level3");
        }
        if (SceneManager.GetActiveScene().name == "level3")
        {
            LevelCompleted.text = "Game Completed!";
            LevelCompleted.enabled = true;
            Invoke("levelLoadDelay2", 6);
        }
    }

    public void levelLoadDelay2()
    {
         SceneManager.LoadScene("mainmenu");
    }

    public void LoadTanks(string level, int loop)
    {
        if (SceneManager.GetActiveScene().name == level)
        {
            //ADD 5 TANKS in LVL1
            for (int i = 0; i < loop; i++)
            {
                Tank enemyTank = new Tank();
                enemyTank.Start("EnemyTank");
                //random (100, 450, 325, 332);
                enemyTank.MainObject.transform.position = new Vector3(Random.Range(100, 450), 0, Random.Range(100, 450));
                enemyTank.MainObject.name = "Enemy Tank " + i;
                m_Tanks.Add(enemyTank);
            }
        }
    }

    /*
	int random(int min,int max,int except1,int except2)
	{
		int[] rnd = new int[10];
		for (int i = 0; i < 10; i++)
		{
			
		    rnd[i] = Random.Range (min, max);
			if(rnd[i]==except1)
			{
				rnd[i] = (rnd[i]+2);
			}
			if(rnd[i]==except2)
			{
				rnd[i] = (rnd[i]+2);
			}
			return rnd[i];
		}

	}
	*/
}