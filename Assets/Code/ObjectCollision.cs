﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ObjectCollision : MonoBehaviour
{
    public Transform EnemyexplosionPrefab;
    public Transform explosionPrefab;
    public bool tankball;
    public bool enemytankball;
    public bool Ammo;
    public bool Hp;
    public static Text AmmoSignal;
    public static Text HpSignal;

    private void Start()
    {
        AmmoSignal = GameObject.Find("AmmoSignal").GetComponent<Text>();
        HpSignal   = GameObject.Find("HpSignal").GetComponent<Text>();        
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (tankball)
        {
            if (collision.gameObject.tag == "EnemyTank")
            {
                Destroy(gameObject, 0.008f); //0.009
                var mainexplosion = (Transform)Instantiate(explosionPrefab, transform.position, transform.rotation);
                Destroy(mainexplosion.gameObject, 3);
            }
            if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "terrain_walls" || collision.gameObject.tag == "mountrock" || collision.gameObject.tag == "Tree" || collision.gameObject.tag == "Object")
            {
                Destroy(gameObject);
                var mainexplosion = (Transform)Instantiate(explosionPrefab, transform.position, transform.rotation);
                Destroy(mainexplosion.gameObject, 3);
            }
        }
        if (enemytankball)
        {
            if (collision.gameObject.name == "Tank")
            {
                Destroy(gameObject, 0.009f); //0.009
                var explosion = (Transform)Instantiate(EnemyexplosionPrefab, transform.position, transform.rotation);
                Destroy(explosion.gameObject,3);
            }
            if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "terrain_walls" || collision.gameObject.tag == "mountrock" || collision.gameObject.tag == "Tree" || collision.gameObject.tag == "Object")
            {
                Destroy(gameObject);
                var explosion = (Transform)Instantiate(EnemyexplosionPrefab, transform.position, transform.rotation);
                Destroy(explosion.gameObject,3);
            }
        }

    }
    void OnTriggerEnter(Collider colltrig)
    {
        if (Ammo)
        {
            if (colltrig.gameObject.name == "Tank")
            {
                if (Tank.Ammo < 100)
                {
                    Destroy(gameObject, 1f); //0.009

                    Tank.Ammo += 40;
                    Tank.GUI_AmmoBar.fillAmount += 0.4f;
                    Tank.GUI_AmmoText.text = Tank.ammo1 + Tank.Ammo.ToString() + " %";
                }
                else if (Tank.Ammo == 100)
                { 
                    AmmoSignal.enabled = true;
                }
                if (Tank.Ammo > 100)
                {
                    Tank.Ammo = 100;
                    Tank.GUI_AmmoText.text = Tank.ammo1 + Tank.Ammo.ToString() + " %";
                }
            }          
        }
        if (Hp)
        {
            if (colltrig.gameObject.name == "Tank")
            {
                if (Tank.Hp <= 50)
                {
                    Destroy(gameObject); //0.009

                    Tank.Hp += 30;
                    Tank.GUI_HpBar.fillAmount += 0.3f;
                    Tank.GUI_HpText.text = Tank.hp1 + Tank.Hp.ToString() + " %";
                }
                else if (Tank.Hp > 50)
                {
                    HpSignal.enabled = true;
                }
                if (Tank.Hp > 100)
                {
                    Tank.Hp = 100;
                    Tank.GUI_HpText.text = Tank.hp1 + Tank.Hp.ToString() + " %";
                }
            }
        }
    }
    void OnTriggerExit(Collider colltrig)
    {
         if (colltrig.gameObject.name == "Tank")
         {  
             AmmoSignal.enabled = false;
             HpSignal.enabled = false;
        }
    }

    void DeleteGameObj(string TankType,ref Transform explosionPrefab,ref Collision collision)
    {
        if (collision.gameObject.tag == TankType)
        {
            Destroy(gameObject, 0.008f); //0.009
            var mainexplosion = (Transform)Instantiate(explosionPrefab, transform.position, transform.rotation);
            Destroy(mainexplosion.gameObject, 3);
        }
        if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "terrain_walls" || collision.gameObject.tag == "mountrock" || collision.gameObject.tag == "Tree")
        {
            Destroy(gameObject);
            var mainexplosion = (Transform)Instantiate(explosionPrefab, transform.position, transform.rotation);
            Destroy(mainexplosion.gameObject, 3);
        }

    }
}