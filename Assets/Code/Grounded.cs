﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grounded : MonoBehaviour 
{
	public  bool  isgrounded   = true;
	private bool  watercollide = false;
    public float  distToGround = 3.10f;
    private Transform middletank;

    private void Start()
    {
        middletank = GameObject.Find("Tank").transform.Find("middletank");
    }

    private void Update()
    {     
        if (Physics.Raycast(middletank.transform.position, Vector3.down, distToGround) )
        {
            isgrounded = true;
            Debug.DrawRay(middletank.transform.position, Vector3.down * distToGround, Color.green); //Sxediazei mia prassinh grammh 3 monadwn pros ta katw an einai grounded
        }
        else if (!(Physics.Raycast(middletank.transform.position, Vector3.down, distToGround)) )
        {
            isgrounded = false;
            Debug.DrawRay(middletank.transform.position, Vector3.down * distToGround, Color.red);//Sxediazei mia kokkinh grammh 3 monadwn pros ta katw an einai grounded
        }

        if ((this.transform.localPosition.y>4.60f )||(Input.GetKeyDown(KeyCode.W) && this.transform.localPosition.y > 4.60f))
        {
            //Debug.Log("OVER 4.60"+ "y pos: " + this.transform.localPosition.y);
            isgrounded = false;
        }
        else if (this.transform.localPosition.y > 0.9f)
        {
            distToGround = 3.29f;
        }
        else if (this.transform.localPosition.y < 0.9f)
        {
            distToGround = 3.10f;
        }

        if (!isgrounded && watercollide)
        {
            distToGround = 4.30f;
        }
    }
    void OnTriggerEnter(Collider colltrig)
    {
        if(colltrig.gameObject.tag == "Water")
        {
            watercollide = true;
            Debug.Log("watercollide = " + watercollide);
        }
    }
    void OnTriggerStay(Collider colltrig)
    {
        if (colltrig.gameObject.tag == "Water")
        {
            watercollide = true;
            Debug.Log("watercollideStay = " + watercollide);
        }
    }
    void OnTriggerExit(Collider colltrig)
    {
        if (colltrig.gameObject.tag == "Water")
        {
            watercollide = false;
            Debug.Log("watercollideExit = " + watercollide);
            distToGround = 3.10f;
        }
    }
    /* //Grounded Using Collisions with terrain
    void OnCollisionEnter(Collision Col)
    {
        if(Col.gameObject.tag == "Ground")
        {
            isgrounded = true;
        }
    }
    void OnCollisionExit(Collision Col)
    {
        if(Col.gameObject.tag == "Ground")
        {
            isgrounded = false;
        }
    }
    */
}
