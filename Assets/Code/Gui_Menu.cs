﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Gui_Menu : MonoBehaviour
{
    public GameObject Gui_Menu1;
    GameObject RestartButton;
    GameObject GraphicsButton;
    GameObject AudioButton;
    GameObject ExitMenuButton;
    GameObject ExitButton;
    GameObject BackGraphicsButton;
    GameObject BackAudioButton;
    GameObject Graphics_FButton;
    GameObject Graphics_BButton;
    GameObject Graphics_GButton;
    GameObject Graphics_SButton;
    GameObject Audio_Off_Button;
    public Text Main;
    GameObject cursor;
    public CursorHandler cursorHandler;
    public static bool menu;

    // Use this for initialization
    public void Start ()
    {
        Gui_Menu1.gameObject.SetActive(false);
        /*
        GameobjectFind3Transform(RestartButton      , "RestartLevel");
        GameobjectFind3Transform(GraphicsButton     , "Graphics");
        GameobjectFind3Transform(AudioButton        , "Audio");
        GameobjectFind3Transform(ExitMenuButton     , "ExitMenu");
        GameobjectFind3Transform(ExitButton         , "Exit");
        GameobjectFind3Transform(BackAudioButton    , "BackAudio");
        GameobjectFind3Transform(BackGraphicsButton , "BackGraphics");
        GameobjectFind3Transform(Graphics_FButton   , "Graphics_F");
        GameobjectFind3Transform(Graphics_BButton   , "Graphics_B");
        GameobjectFind3Transform(Graphics_GButton   , "Graphics_G");
        GameobjectFind3Transform(Graphics_FButton   , "Graphics_F");
        GameobjectFind3Transform(Graphics_SButton   , "Graphics_S");
        GameobjectFind3Transform(Audio_Off_Button   , "Audio_Off");
        */

        RestartButton = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find("RestartLevel").gameObject;
        GraphicsButton = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find("Graphics").gameObject;
        AudioButton = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find("Audio").gameObject;
        ExitMenuButton = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find("ExitMenu").gameObject;
        ExitButton = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find("Exit").gameObject;
        BackAudioButton = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find("BackAudio").gameObject;
        BackGraphicsButton = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find("BackGraphics").gameObject;
        Graphics_FButton = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find("Graphics_F").gameObject;
        Graphics_BButton = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find("Graphics_B").gameObject;
        Graphics_GButton = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find("Graphics_G").gameObject;
        Graphics_SButton = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find("Graphics_S").gameObject;
        Audio_Off_Button = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find("Audio_Off").gameObject;

        cursor = GameObject.Find("Cursor_Handler");
        cursorHandler = cursor.GetComponent<CursorHandler>();

        BackAudioButton.SetActive(false);
        BackGraphicsButton.SetActive(false);
        Graphics_FButton.SetActive(false);
        Graphics_BButton.SetActive(false);
        Graphics_GButton.SetActive(false);
        Graphics_SButton.SetActive(false);
        Audio_Off_Button.SetActive(false);
        menu = false;
    }

	// Update is called once per frame
	public void Update ()
    {
        if (Gui_Menu1.gameObject.activeSelf==false && Input.GetKeyDown(KeyCode.Escape))
        {
            menu = true;
            Gui_Menu1.gameObject.SetActive(true);
            Time.timeScale = 0;
            if(menu == true)
            {
                Cursor.visible = true;
            }
        }
        else if (Gui_Menu1.gameObject.activeSelf == true && Input.GetKeyDown(KeyCode.Escape))
        {
            menu = false;
            Gui_Menu1.gameObject.SetActive(false);
            Time.timeScale = 1;
            if (menu == false)
            {
                Cursor.visible = false;
            }
        }

    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void Graphics()
    {
        Main.text = "Graphics Menu :";
        RestartButton.SetActive(false);
        GraphicsButton.SetActive(false);
        GraphicsButton.SetActive(false);
        AudioButton.SetActive(false);
        ExitMenuButton.SetActive(false);
        ExitButton.SetActive(false);
        BackGraphicsButton.SetActive(true);
        BackAudioButton.SetActive(false);

        Graphics_FButton.SetActive(true);
        Graphics_BButton.SetActive(true);
        Graphics_GButton.SetActive(true);
        Graphics_SButton.SetActive(true);
    }
    public void Graphics_F()
    {
        QualitySettings.SetQualityLevel(5);
    }
    public void Graphics_B()
    {
        QualitySettings.SetQualityLevel(4);
    }
    public void Graphics_G()
    {
        QualitySettings.SetQualityLevel(3);
    }
    public void Graphics_S()
    {
        QualitySettings.SetQualityLevel(2);
    }
    public void Audio()
    {
        Main.text = "Audio Menu :";
        RestartButton.SetActive(false);
        GraphicsButton.SetActive(false);
        GraphicsButton.SetActive(false);
        AudioButton.SetActive(false);
        ExitMenuButton.SetActive(false);
        ExitButton.SetActive(false);
        BackAudioButton.SetActive(true);
        BackGraphicsButton.SetActive(false);
        Audio_Off_Button.SetActive(true);
    }
    public void AudioOff()
    {
        AudioListener.volume = 0;
    }
    public void BackAudio()
    {
        Main.text = "Menu :";
        RestartButton.SetActive(true);
        GraphicsButton.SetActive(true);
        GraphicsButton.SetActive(true);
        AudioButton.SetActive(true);
        ExitMenuButton.SetActive(true);
        ExitButton.SetActive(true);
        BackAudioButton.SetActive(false);
        Audio_Off_Button.SetActive(false);
    }
    public void BackGraphics()
    {
        Main.text = "Menu :";
        RestartButton.SetActive(true);
        GraphicsButton.SetActive(true);
        GraphicsButton.SetActive(true);
        AudioButton.SetActive(true);
        ExitMenuButton.SetActive(true);
        ExitButton.SetActive(true);
        BackGraphicsButton.SetActive(false);

        Graphics_FButton.SetActive(false);
        Graphics_BButton.SetActive(false);
        Graphics_GButton.SetActive(false);
        Graphics_SButton.SetActive(false);
    }
    public void ExittoMain()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void ExitGame()
    {
        Application.Quit();
    }

    public void GameobjectFind3Transform(GameObject o_Button,string button)
    {
        o_Button = GameObject.Find("Canvas").transform.Find("Menu").transform.Find("Panel").transform.Find(button).gameObject;
    }
}