﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fell_Handler : MonoBehaviour
{
    public GameObject tank;

    public void OnTriggerEnter(Collider Col)
    {
        if (Col.gameObject.tag == "Ground")
        {
           //Debug.Log("UPPERTANK COLLIDED WITH THE GROUND");
           tank.transform.rotation =  new Quaternion(0, 0, 0 ,0);
        }
    }
}