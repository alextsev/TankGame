﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_alter : MonoBehaviour 
{
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKey(KeyCode.Alpha1))	
		{
            ChangeCamera(-0.6f, 31f, -35f, 21.108f, 0f, 0f);
		}
		if(Input.GetKey(KeyCode.Alpha2))	
		{
            ChangeCamera(-0.6f, 23.7f, -11.5f, 21.108f, 0f, 0f);
		}
		if(Input.GetKey(KeyCode.Alpha3))	
		{
            ChangeCamera(0f, 16.5f, 9.3f, 21.108f, 0f, 0f);
		}
		if(Input.GetKey(KeyCode.Alpha4))	
		{
            ChangeCamera(45f, 31f, 3f, 21.108f, -90f, 0f);
		}
        if (Input.GetKey(KeyCode.Alpha5))
        {
            ChangeCamera(-0.6f, 31f, 35f, 21.108f, 180f, 0f);
        }
    }

    void ChangeCamera(float posX, float posY, float posZ, float rotX, float rotY, float rotZ)
    {
        transform.localPosition = new Vector3(posX, posY, posZ);
        transform.localRotation = Quaternion.Euler(rotX, rotY, rotZ);
    }
}
