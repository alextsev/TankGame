﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tank
{
    private float m_MovementSpeed;
    public static float m_TargetMovementSpeed;
    private float m_MovementSpeedVel;

    private float m_RotationSpeed;
    private float m_TargetRotationSpeed;
    private float m_RotationSpeedVel;
    private Rigidbody m_Rigidbody;

    public GameObject MainObject;
    public static Image GUI_HpBar;
    public static Image GUI_AmmoBar;
    public static Image GUI_OverloadBar;
    public static Text GUI_HpText;
    public static Text GUI_AmmoText;
    public static Text GUI_OverloadText;
    public static int Hp = 100;
    public static int Ammo = 100;
    public static int Overload = 100;
    public static string hp1 = "Health                  |";
    public static string ammo1 = "Ammo                  |";
    public static string overload1 = "Overload              |";
    public static Text GameOver;
    public static Text LevelCompleted;
    public static Text OverloadSignal;

    //sound
    AudioSource m_MyAudioSource;
    public AudioClip canon_movement1;

    public PlayerCanonAttack playerCanonAttack;
    public Grounded grounded1;
    public GameObject object1;

    private Renderer m_TrackLeft, m_TrackRight;
    private const float TRACK_SPEED = 0.5f;


    private const float NORMAL_SPEED             =  10.0f;
    private const float FAST_SPEED               =  16.0f;
    private const float SLOW_SPEED               =  14.0f;
    private const float NORMAL_ROTATION_SPEED    =  1.4f;


    public void Start(string objectName)
    {
        Time.timeScale = 1;
        MainObject = Object.Instantiate(Resources.Load<GameObject>(objectName));
        m_Rigidbody = MainObject.GetComponent<Rigidbody>();
        m_MyAudioSource = MainObject.GetComponent<AudioSource>();
        canon_movement1 = (AudioClip)Resources.Load("sounds/canonfire1");
       // GameObject canon = GameObject.Find("canon");
       // playerCanonAttack = canon.GetComponent<PlayerCanonAttack>();

        grounded1 = MainObject.GetComponent<Grounded>();

        #region FindTracks
        Transform[] transforms = MainObject.GetComponentsInChildren<Transform>();

        for (int i = 0; i < transforms.Length; i++)
        {
            if (transforms[i].name == "trackleft")
            {
                m_TrackLeft = transforms[i].GetComponent<Renderer>();
            }
            else if (transforms[i].name == "trackright")
            {
                m_TrackRight = transforms[i].GetComponent<Renderer>();
            }
            else if (transforms[i].name == "canon")
            {
                GameObject canon = transforms[i].gameObject;// GameObject.Find("canon");
                playerCanonAttack = canon.GetComponent<PlayerCanonAttack>();
            }
        }

        m_TrackRight.material.mainTextureOffset = Vector2.zero;
        m_TrackLeft.material.mainTextureOffset = Vector2.zero;

        #endregion

        GUI_HpBar = GameObject.Find("hp_bar_fill").GetComponent<Image>();
        GUI_AmmoBar = GameObject.Find("ammo_bar_fill").GetComponent<Image>();
        GUI_OverloadBar = GameObject.Find("overload_bar_fill").GetComponent<Image>();
        GUI_HpText = GameObject.Find("tank_health_text").GetComponent<Text>();
        GUI_AmmoText = GameObject.Find("tank_ammo_text").GetComponent<Text>();
        GUI_OverloadText = GameObject.Find("tank_overload_text").GetComponent<Text>();
        GUI_HpText.text = hp1 + Hp.ToString() + " %";
        GUI_AmmoText.text = ammo1 + Ammo.ToString() + " %";
        GUI_OverloadText.text = overload1 + Overload.ToString() + " %";

        object1 = GameObject.Find("mountrock2");
        GameOver = GameObject.Find("GameOver").GetComponent<Text>();
        OverloadSignal = GameObject.Find("OverloadSignal").GetComponent<Text>();
        LevelCompleted = GameObject.Find("LevelCompleted").GetComponent<Text>();

        GameOver.enabled = false;
        Hp = 100;
        Ammo = 100;
        Overload = 100;
    }

    public virtual void Update()
    {
        #region AUDIO

        if (m_Rigidbody.velocity.magnitude <= 0.2)
        {
            m_MyAudioSource.Play();
        }

        #endregion

        if (grounded1.isgrounded == true)
        {
            if (Input.GetKey(KeyCode.W))
            {
                //print("w key was pressed");
                m_TargetMovementSpeed = NORMAL_SPEED;
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                {
                    m_TargetMovementSpeed = FAST_SPEED;
                }
            }
            else if (Input.GetKey(KeyCode.S))
            {
                //print("s key was pressed");
                m_TargetMovementSpeed = -NORMAL_SPEED;
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                {
                    m_TargetMovementSpeed = -SLOW_SPEED;
                }
            }
            else
            {
                m_TargetMovementSpeed = 0;
            }

            if ( Input.GetKey(KeyCode.D) && !Gui_Menu.menu )
            {
                //print("d key was pressed");
                m_TargetRotationSpeed = NORMAL_ROTATION_SPEED;
            }
            else if ( Input.GetKey(KeyCode.A)  && !Gui_Menu.menu)
            {
                //print("a key was pressed");
                m_TargetRotationSpeed = -NORMAL_ROTATION_SPEED;
            }
            else
            {
                m_TargetRotationSpeed = 0;
            }

            m_MovementSpeed = Mathf.SmoothDamp(m_MovementSpeed, m_TargetMovementSpeed, ref m_MovementSpeedVel, 0.1f);
            m_RotationSpeed = Mathf.SmoothDamp(m_RotationSpeed, m_TargetRotationSpeed, ref m_RotationSpeedVel, 0.1f);

            if (m_MovementSpeed > 0.1f || m_MovementSpeed < -0.1f)
            {
                m_Rigidbody.velocity = MainObject.transform.forward * m_MovementSpeed;
            }
            if (m_RotationSpeed > 0.1f || m_RotationSpeed < -0.1f)
            {
                MainObject.transform.rotation = Quaternion.Euler(MainObject.transform.rotation.eulerAngles + new Vector3(0, m_RotationSpeed, 0));
            }
        }

        //HP_Handler
        if (Hp <= 0)
        {
            Hp = 0;
            GameOver.enabled = true;
            Time.timeScale = 0;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        //Overload-Ammo FillBar 
        if (GUI_OverloadBar.fillAmount < 0.1f)
        {
            GUI_OverloadBar.fillAmount = 0;
        }
        if (GUI_AmmoBar.fillAmount < 0.09f)
        {
            GUI_AmmoBar.fillAmount = 0;
        }
        //Overload_Handler
        if (Overload == 0)
        {
            OverloadSignal.enabled = true;
        }
        else
        {
            OverloadSignal.enabled = false;
        }

        if ((Input.GetKey(KeyCode.Space) || (Input.GetMouseButton(0))) && playerCanonAttack.shootOn)
        {
            //GUI
            GUI_AmmoBar.fillAmount -= 0.100f;
            GUI_OverloadBar.fillAmount -= 0.200f;
            Ammo -= 10;
            Overload -= 20;
            GUI_AmmoText.text = ammo1 + Ammo.ToString() + " %";
            GUI_OverloadText.text = overload1 + Overload.ToString() + " %";
        }

        #region TRACKS

        if (m_TargetMovementSpeed == NORMAL_SPEED)
        {
            MoveTracks(Time.deltaTime, m_TargetMovementSpeed, NORMAL_SPEED);
        }
        else if (m_TargetMovementSpeed == -NORMAL_SPEED)
        {
            MoveTracks(-Time.deltaTime, m_TargetMovementSpeed, -NORMAL_SPEED);
        }
        else if (m_TargetMovementSpeed == FAST_SPEED)
        {
            MoveTracks(Time.deltaTime,  m_TargetMovementSpeed * 20, FAST_SPEED);
        }
        else if (m_TargetMovementSpeed == -SLOW_SPEED)
        {
            MoveTracks(-Time.deltaTime, m_TargetMovementSpeed * 15, -SLOW_SPEED);
        }
        else if (m_TargetMovementSpeed == 0)
        {
            //noMovement
        }

        #endregion

        //change Map
        if (Input.GetKey(KeyCode.F1) || Input.GetKey(KeyCode.K))
        {
            Time.timeScale = 1;
            GameOver.enabled = false;
            Hp = 100;
            Ammo = 100;
            Overload = 100;
            SceneManager.LoadScene("level1");
        }
        if (Input.GetKey(KeyCode.F2) || Input.GetKey(KeyCode.L))
        {
            Time.timeScale = 1;
            GameOver.enabled = false;
            Hp = 100;
            Ammo = 100;
            Overload = 100;
            SceneManager.LoadScene("level2");
        }
        if (Input.GetKey(KeyCode.F3))
        {
            Time.timeScale = 1;
            GameOver.enabled = false;
            Hp = 100;
            Ammo = 100;
            Overload = 100;
            SceneManager.LoadScene("level3");
        }
    
    }

    void MoveTracks(float Time1, float m_TargetMovementSpeed1, float DIVIDE_SPEED)
    {
        m_TrackRight.material.mainTextureOffset += new Vector2(0, Time1 * TRACK_SPEED * m_TargetMovementSpeed1 / DIVIDE_SPEED);
        m_TrackLeft.material.mainTextureOffset  += new Vector2(0, Time1 * TRACK_SPEED * m_TargetMovementSpeed1 / DIVIDE_SPEED);
    }

}