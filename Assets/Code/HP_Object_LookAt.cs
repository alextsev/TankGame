﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP_Object_LookAt : MonoBehaviour
{
    Camera target;
    private Transform HP;

    void Start ()
    {
        target = Camera.main;
        HP = transform;
    }

	void Update ()
    {
        if (HP)
        { 
            HP.transform.LookAt(target.transform);
            HP.transform.localEulerAngles += new Vector3(0f, 180, 0f);
        }
    }
}
