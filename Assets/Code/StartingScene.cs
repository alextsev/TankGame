﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartingScene : MonoBehaviour
{

    float timer;
    int int_timer;
    public GameObject human1;
    public Transform enemy;
    StartEnemyFollow startenemyFollow;
    public Text textPrepare;
    float startTime;

    // Use this for initialization
    void Start ()
    {
        enemy = GameObject.Find("Enemy_Tank_Menu").transform.Find("uppertank1");     
        startenemyFollow = enemy.GetComponent<StartEnemyFollow>();
        textPrepare = GameObject.Find("Canvas").transform.Find("Text").GetComponent<Text>();
        textPrepare.enabled = false;
        //int startTime = (int)Time.time;
        startTime = Time.time;

    }

    // Update is called once per frame
    void Update()
    {
        //timer = (int)Time.time;
        timer = Time.time-startTime;
        int_timer = (int)timer;

        if (int_timer == 10)
        {
            startenemyFollow.enabled = false;
            Destroy(human1);
        }
        if (int_timer == 11)
        {
            textPrepare.enabled = true;
        }
        if (int_timer == 16)
        {
            SceneManager.LoadScene("level1");
        }
    }
}