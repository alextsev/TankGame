﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHPLookAt : MonoBehaviour 
{
	Camera cameraTOLookAt;
    GameObject MyTank;

	// Use this for initialization
	void Start () 
	{
		cameraTOLookAt = Camera.main;
		MyTank = transform.parent.gameObject;
		transform.SetParent ( null );
	    //cameraTOLookAt = GameObject.Find ("Main Camera").GetComponent<Camera> ();
	}
	// Update is called once per frame
	void Update () 
	{
		//transform.LookAt (cameraTOLookAt.transform.rotation *Vector3.back, cameraTOLookAt.transform.rotation *Vector3.up);
		transform.LookAt ( cameraTOLookAt.transform );
		transform.localEulerAngles += new Vector3 ( 0f, 180f, 0f );

		//transform.position = new Vector3 ( transform.position.x, 8f, transform.position.z );
		if (Vector3.Dot (MyTank.transform.up, Vector3.down) > 0)
		{
			transform.position = MyTank.transform.position + new Vector3 (0f, 3f, 0f);
		}
		else
		{
			transform.position = MyTank.transform.position + new Vector3 ( 0f, 6f, 0f );
		}

/*
		if (Vector3.Dot( transform.up, Vector3.down ) > 0)
		{
			Gui.transform.localPosition = new Vector3 ( 0f, -9f, 0f );
		}
		else
		{
			Gui.transform.localPosition = new Vector3 ( 0f, 23f, 0f );
		}
*/
    }
}
