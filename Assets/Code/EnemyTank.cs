﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyTank : MonoBehaviour
{
    private int HP = 100;
    public Text Health1;
    public Image hpbar;
    public GameObject Gui;
    //int[] caseSwitch = new int[10];
    public int caseSwitch;
    public int caseSwitch2;
    float  enemytankspeed = 6f;
    public GameObject Waypoints;
    public Transform waypoint1;
    public Transform waypoint2;
    public Transform waypoint3;
    public Transform waypoint4;
    public Transform waypoint5;
    public Transform waypoint6;
    public Transform waypoint7;
    public Transform waypoint8;
    public Transform waypoint9;
    public Transform waypoint10;
    public Transform LookAtTarget;
    public Transform LookAtFollowPointTarget;
    public Transform uppertank;
    public Transform explosion_Tank;
    public static Grounded grounded1;
    public bool enemyTankstartPosition;
    public bool enemyTanknewPosition = false;
    public bool enemyTankHunting     = false;
    public bool enemyTankIdlecase    = false;
    public bool enemyTankAttacked    = false;
    private Renderer m_enemy_TrackLeft, m_enemy_TrackRight;
    private const float ENEMY_TRACK_SPEED = 0.5f;
    private string score = "Score: ";
    public static int counter = 0;
    public Text scoreText;

    public void Start()
    {
        //GameObject g = GameObject.Find ("EnemyTankHP");
        //Health1 = GameObject.Find ("EnemyTankHP").GetComponent<TextMesh>();
        //Health = HP.ToString ();
        //bar = GameObject.Find ("hpbar2");
        Health1.text = HP.ToString() + " %";
        //		hpbarfill = hpbarfill.ToString ();
        LookAtFollowPointTarget = GameObject.Find("Tank").transform.Find("FollowPoint");
        uppertank = this.transform.Find("middletank").transform.Find("uppertank1");
        LookAtTarget = GameObject.Find("Tank").transform.Find("middletank").transform.Find("uppertank1");
        scoreText = GameObject.Find("Canvas").transform.Find("Score").GetComponent<Text>();
        counter = 0;
        scoreText.text = score + counter;

        Transform[] transforms = this.GetComponentsInChildren<Transform>();
        for (int i = 0; i < transforms.Length; i++)
        {
            if (transforms[i].name == "trackleft")
            {
                m_enemy_TrackLeft = transforms[i].GetComponent<Renderer>();
            }
            else if (transforms[i].name == "trackright")
            {
                m_enemy_TrackRight = transforms[i].GetComponent<Renderer>();
            }
        }

        //WAYPOINTS
        Waypoints  = GameObject.Find("EnemyWaypoints");
        waypoint1  = Waypoints.transform.Find("WP1");
        waypoint2  = Waypoints.transform.Find("WP2");
        waypoint3  = Waypoints.transform.Find("WP3");
        waypoint4  = Waypoints.transform.Find("WP4");
        waypoint5  = Waypoints.transform.Find("WP5");
        waypoint6  = Waypoints.transform.Find("WP6");
        waypoint7  = Waypoints.transform.Find("WP7");
        waypoint8  = Waypoints.transform.Find("WP8");
        waypoint9  = Waypoints.transform.Find("WP9");
        waypoint10 = Waypoints.transform.Find("WP10");

        /*aris
        AvailableWaypoints = new Transform[]
        {
            waypoint1,
            waypoint2,
            waypoint3,
            waypoint4,
            waypoint5,
            waypoint6,
            waypoint7,
            waypoint8,
            waypoint9,
            waypoint10,
        };
        */
        caseSwitch  = Random.Range(1, 12);
        caseSwitch2 = Random.Range(1, 12);

        //m_TargetWaypoint = AvailableWaypoints[caseSwitch];//aris
        enemyTankstartPosition = true;
        //Debug.Log("EDW : " + caseSwitch);

    }
    // Update is called once per frame
    public void Update()
    {
        if (HP < 0)
        {
            HP = 0;
        }
        if (HP == 0)
        {
            Destroy(gameObject);
            var explosionTank = (Transform)Instantiate(explosion_Tank, transform.position, transform.rotation);
            Destroy(explosionTank.gameObject, 3);
            Destroy(Gui);
            //Counter Score
            counter++;
            scoreText.text = score + counter;
        }

        //safe respawn tank if it falls because of a bug
        if (this.transform.localPosition.y < -50)
        {
            this.transform.localPosition = new Vector3(Random.Range(100, 450), 0, Random.Range(100, 450));
        }

        //Wheels Animation Handler
        if (enemyTanknewPosition == true || enemyTankstartPosition == true || enemyTankHunting == true)
        {
            this.gameObject.GetComponent<Animation>().enabled = true;
        }
        else
        {
            this.gameObject.GetComponent<Animation>().enabled = false;
        }

        //Ypologismos apostashs otan to enemyTank koda sto Tank
        float distfollow = Vector3.Distance(LookAtFollowPointTarget.transform.position, transform.localPosition);
        distfollow      += 50;

        if (enemyTankstartPosition == true)
        {
            switch (caseSwitch)
            {
                case 1:
                    //Vector3 newDir1 = Vector3.RotateTowards(transform.forward, waypoint1.position - transform.position, enemytankspeed * Time.deltaTime, 0.0f);
                    //transform.localRotation = Quaternion.LookRotation(newDir1);
                    /*
                    float dist1 = Vector3.Distance(waypoint1.position, transform.localPosition);
                    if (dist1 >= 30)
                    {
                        var newDir1 = Quaternion.LookRotation(waypoint1.position - transform.position);
                        transform.localRotation = Quaternion.Slerp(transform.localRotation, newDir1, Time.deltaTime * 10);
                        transform.localPosition = Vector3.MoveTowards(transform.localPosition, waypoint1.position, enemytankspeed * Time.deltaTime);
                    }
                    else
                    {
                        enemyTankstartPosition = false;
                    }
                    */
                    //Move(m_TargetWaypoint.position, ref enemyTankstartPosition);//aris
                    Move(waypoint1.position, ref enemyTankstartPosition); 
                    break;               
                case 2:
                    //this.transform.Translate(Vector3.back * enemytankspeed * Time.deltaTime, Space.World);
                    Move(waypoint2.position, ref enemyTankstartPosition);
                    break;
                case 3:
                    Move(waypoint3.position, ref enemyTankstartPosition);
                    break;
                case 4:
                    Move(waypoint4.position, ref enemyTankstartPosition);
                    break;
                case 5:
                    Move(waypoint5.position, ref enemyTankstartPosition);
                    break;
                case 6:
                    Move(waypoint6.position, ref enemyTankstartPosition);
                    break;
                case 7:
                    Move(waypoint7.position, ref enemyTankstartPosition);
                    break;
                case 8:
                    Move(waypoint8.position, ref enemyTankstartPosition);
                    break;
                case 9:
                    Move(waypoint9.position, ref enemyTankstartPosition);
                    break;
                case 10:
                    Move(waypoint10.position, ref enemyTankstartPosition);
                    break;
                case 11:
                    enemyTankIdlecase = true;
                    break;
            }
        }
        else if (enemyTankstartPosition == false && enemyTankHunting == false && distfollow > 100)
        {
            enemyTanknewPosition = true;
            NewPosition();
        }

        if (distfollow <= 90)
        {
            enemyTankstartPosition  = false;
            enemyTanknewPosition    = false;
            enemyTankHunting        = true;
            var newDir11            = Quaternion.LookRotation(LookAtFollowPointTarget.position - transform.position);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, newDir11, Time.deltaTime * 10);
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, LookAtFollowPointTarget.position, enemytankspeed * Time.deltaTime);
            MoveEnemyTracks();
            //transform.localPosition = transform.localPosition + 5;
            /*
            //gia uppertank
            var rotate = Quaternion.LookRotation(LookAtTarget.position - uppertank.transform.position);
            transform.rotation = Quaternion.Slerp(uppertank.transform.rotation, rotate, Time.deltaTime * 10);
            */
        }
        else if (enemyTankAttacked == true)
        {
            float distfollowattacked = Vector3.Distance(LookAtFollowPointTarget.transform.position, transform.localPosition);
            distfollowattacked      += 50;
            enemyTankstartPosition   = false;
            enemyTanknewPosition     = false;
            enemyTankHunting         = true;
            var newDir12             = Quaternion.LookRotation(LookAtFollowPointTarget.position - transform.position);
            transform.localRotation  = Quaternion.Slerp(transform.localRotation, newDir12, Time.deltaTime * 10);
            transform.localPosition  = Vector3.MoveTowards(transform.localPosition, LookAtFollowPointTarget.position, enemytankspeed * Time.deltaTime);
            MoveEnemyTracks();

            if (distfollowattacked > 150)
            {
                enemyTankHunting     = false;
                enemyTankAttacked    = false;
            }
        }
        else
        {
            enemyTankHunting         = false;
        }
    }

    private void Move(Vector3 position, ref bool isGoingToNewPosition)
    {
        //this.transform.Translate(Vector3.back * enemytankspeed * Time.deltaTime, Space.World);
        float dist2 = Vector3.Distance(position, transform.localPosition);
        if (dist2 >= 30)
        {
            Quaternion newDir2      = Quaternion.LookRotation(position - transform.position);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, newDir2, Time.deltaTime * 10);
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, position, enemytankspeed * Time.deltaTime);
            MoveEnemyTracks();
        }
        else
        {
            isGoingToNewPosition    = false;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Tankball")
        {
            this.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            HP               -= 20;
            Health1.text      = HP.ToString() + " %";
            hpbar.fillAmount -= 0.2f;

            enemyTankAttacked = true;
        }
        if (collision.gameObject.tag == "Ground")
        {
            //Gui.transform.localPosition = new Vector3 (-2.3f,-9f,2f);
        }
    }
    void OnCollisionExit(Collision collision)
    {
    }
    void NewPosition()
    {
        if (enemyTanknewPosition == true)
        {
            switch (caseSwitch2)
            {
                case 1:
                    //Vector3 newDir1 = Vector3.RotateTowards(transform.forward, waypoint1.position - transform.position, enemytankspeed * Time.deltaTime, 0.0f);
                    //transform.localRotation = Quaternion.LookRotation(newDir1);
                    Move(waypoint1.position, ref enemyTanknewPosition);
                    break;
                case 2:
                    //this.transform.Translate(Vector3.back * enemytankspeed * Time.deltaTime, Space.World);
                    Move(waypoint2.position, ref enemyTanknewPosition);
                    break;
                case 3:
                    Move(waypoint3.position, ref enemyTanknewPosition);
                    break;
                case 4:
                    Move(waypoint4.position, ref enemyTanknewPosition);
                    break;
                case 5:
                    Move(waypoint5.position, ref enemyTanknewPosition);
                    break;
                case 6:
                    Move(waypoint6.position, ref enemyTanknewPosition);
                    break;
                case 7:
                    Move(waypoint7.position, ref enemyTanknewPosition);
                    break;
                case 8:
                    Move(waypoint8.position, ref enemyTanknewPosition);
                    break;
                case 9:
                    Move(waypoint9.position, ref enemyTanknewPosition);
                    break;
                case 10:
                    Move(waypoint10.position, ref enemyTanknewPosition);
                    break;
                case 11:
                    enemyTankIdlecase = true;
                    break;
            }
        }
    }
    void MoveEnemyTracks()
    {
        m_enemy_TrackRight.material.mainTextureOffset += new Vector2(0, Time.deltaTime * ENEMY_TRACK_SPEED * enemytankspeed / enemytankspeed);
        m_enemy_TrackLeft.material.mainTextureOffset  += new Vector2(0, Time.deltaTime * ENEMY_TRACK_SPEED * enemytankspeed / enemytankspeed);
    }
}
