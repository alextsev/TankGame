﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour 
{
	public GameObject MenuStart;
	public GameObject MenuOptions;
	public GameObject MenuGraphics;
	public GameObject MenuAudio;
	public GameObject MenuControls;

	public GameObject button1_Start;
	public GameObject button2_Options;
	public GameObject button3_Controls;
	public GameObject button4_Exit;
	public GameObject button_Graphics;
	public GameObject button_Audio;
	public GameObject button_Back;
	public GameObject button_Quality_F;
	public GameObject button_Quality_B;
	public GameObject button_Quality_G;
	public GameObject button_Quality_S;
	public GameObject buttonBack;
	public GameObject title_Graphic;

	public GameObject button_Full_Audio;
	public GameObject button_AudioMute;
	public GameObject buttonBack2;
    public GameObject title_Audio;

    public GameObject button1_1;
	public GameObject button2_2;
	public GameObject button3_3;
	public GameObject button4_4;

    public GameObject button5_5;
    public GameObject button6_6;
    public GameObject button7_7;
    public GameObject button8_8;

    public GameObject buttonBack_5;
    public GameObject buttonBack_6;
    public GameObject buttonNext;
    public GameObject title_Controls;

	public AudioSource audioSourceSystem;
	public AudioSource audioSourceMusic;
    private bool menu;
    private bool menucontrols;
    private bool menucontrols2;
    private bool menugraphics;

    // Use this for initialization
    void Start () 
	{
		MenuStart 		 = GameObject.Find("MenuStart");
		MenuOptions  	 = GameObject.Find("MenuOptions");
		MenuGraphics	 = GameObject.Find("MenuGraphics");
		MenuAudio   	 = GameObject.Find("MenuAudio");
		MenuControls	 = GameObject.Find("MenuControls");

		button1_Start	 = MenuStart.transform.Find ("button_Start").gameObject;
		button2_Options  = MenuStart.transform.Find ("button2_Options").gameObject;
		button3_Controls = MenuStart.transform.Find("button3_Controls").gameObject;
		button4_Exit     = MenuStart.transform.Find("button4_Exit").gameObject;

		button_Graphics  = MenuOptions.transform.Find ("button_Graphics").gameObject;
		button_Audio     = MenuOptions.transform.Find ("button_Audio").gameObject;
		buttonBack       = MenuOptions.transform.Find("buttonBack").gameObject;
		button_Quality_F = MenuGraphics.transform.Find("button_Quality_F").gameObject;
		button_Quality_B = MenuGraphics.transform.Find("button_Quality_B").gameObject;
		button_Quality_G = MenuGraphics.transform.Find("button_Quality_G").gameObject;
		button_Quality_S = MenuGraphics.transform.Find("button_Quality_S").gameObject;
		button_Back 	 = MenuGraphics.transform.Find ("button_Back").gameObject;
		title_Graphic	 = MenuGraphics.transform.Find("title_Graphic").gameObject;

		button_Full_Audio= MenuAudio.transform.Find("button_Full_Audio").gameObject;
		button_AudioMute = MenuAudio.transform.Find("button_Audio_Mute").gameObject;
		buttonBack2  	 = MenuAudio.transform.Find("button_Back<").gameObject;
        title_Audio      = MenuAudio.transform.Find("title_Audio").gameObject;

        button1_1 		 = MenuControls.transform.Find ("button1_1").gameObject;
		button2_2 		 = MenuControls.transform.Find ("button2_2").gameObject;
		button3_3 		 = MenuControls.transform.Find ("button3_3").gameObject;
		button4_4 		 = MenuControls.transform.Find ("button4_4").gameObject;

        button5_5        = MenuControls.transform.Find("button5_5").gameObject;
        button6_6        = MenuControls.transform.Find("button6_6").gameObject;
        button7_7        = MenuControls.transform.Find("button7_7").gameObject;
        button8_8        = MenuControls.transform.Find("button8_8").gameObject;

        buttonBack_5 	 = MenuControls.transform.Find ("buttonBack_5").gameObject;
        buttonBack_6     = MenuControls.transform.Find ("buttonBack_6").gameObject;
        buttonNext       = MenuControls.transform.Find ("buttonNext").gameObject;
        title_Controls 	 = MenuControls.transform.Find ("title_Controls").gameObject;

	}

	void OnMouseOver()
	{
			//MENU START
			if (gameObject.name == "button_Start") 
			{
                button1_Start.transform.localPosition = new Vector3(-0.30f, 3.34f, 0.0f);
                if (Input.GetMouseButtonDown (0))
				{
					SceneManager.LoadScene("Start");
				}
			} 
			else if (gameObject.name == "button2_Options") 
			{
                 button2_Options.transform.localPosition = new Vector3(-0.30f, 1.9411f, 0.0f);
                //transform.position = new Vector3 (-1, 0, 0) * Time.deltaTime;
                if (Input.GetMouseButtonDown (0))
				{
                    menu = false;
                    BasicMenuTurnBool(ref menu);
                    button_Graphics.SetActive (true);
					button_Audio.SetActive (true);
					buttonBack.SetActive (true);
				}
			} 
			else if (gameObject.name == "button3_Controls") 
			{
                button3_Controls.transform.localPosition = new Vector3(-0.30f, 0.6599998f, 0.0f);
				//transform.position = new Vector3 (-1, 0, 0) * Time.deltaTime;
				if (Input.GetMouseButtonDown (0))
				{
                    menu = false;
                    BasicMenuTurnBool(ref menu);
                    menucontrols2 = true;
                    MenuControls2TurnBool(ref menucontrols2);
                    title_Controls.SetActive (true);
				}
			} 
			else if (gameObject.name == "button4_Exit") 
			{
                button4_Exit.transform.localPosition = new Vector3(-0.30f, -0.6100001f, 0.0f);
                //transform.position = new Vector3 (-1, 0, 0) * Time.deltaTime;
                if (Input.GetMouseButtonDown (0))
				{
					Application.Quit();
				}
			} 
		    else{}

			//MENU OPTIONS
			if (gameObject.name == "button_Graphics") 
			{
                button_Graphics.transform.localPosition = new Vector3(-0.30f, 3.34f, 0.0f);
            if (Input.GetMouseButtonDown (0))
				{
					button_Graphics.SetActive (false);
					button_Audio.SetActive (false);
					buttonBack.SetActive (false);
                    menugraphics = true;
                    MenuGraphicsTurn(ref menugraphics);
                }
			} 
			else if (gameObject.name == "button_Audio") 
			{
                //transform.position = new Vector3 (-1, 0, 0) * Time.deltaTime;
                button_Audio.transform.localPosition = new Vector3(-0.30f, 1.9411f, 0.0f);
                if (Input.GetMouseButtonDown (0))
				{
					button_Graphics.SetActive (false);
					button_Audio.SetActive (false);
                    title_Audio.SetActive(true);
                    buttonBack.SetActive (false);
					button_Full_Audio.SetActive (true);
					button_AudioMute.SetActive (true);
					buttonBack2.SetActive (true);
				}
			} 
			else if (gameObject.name == "buttonBack")
            {
				//print ("button3");
				//transform.position = new Vector3 (-1, 0, 0) * Time.deltaTime;
                buttonBack.transform.localPosition = new Vector3(-0.30f, 0.5916002f, 0.0f);
                if (Input.GetMouseButtonDown (0))
				{
					button_Graphics.SetActive (false);
					button_Audio.SetActive (false);
					buttonBack.SetActive (false);
                    title_Audio.SetActive(false);
                    menu = true;
                    BasicMenuTurnBool(ref menu);
                }
			} 
			else{}

			//MENU AUDIO
			if (gameObject.name == "button_Full_Audio")
            {
                button_Full_Audio.transform.localPosition = new Vector3(-0.30f, 3.06f, 0.0f);
                if (Input.GetMouseButtonDown (0))
				{
                    AudioListener.volume = 0.6f;
                }
            } 
			if (gameObject.name == "button_Audio_Mute") 
			{
                button_AudioMute.transform.localPosition = new Vector3(-0.30f, 1.94f, 0.0f);
                if (Input.GetMouseButtonDown (0))
				{
					AudioListener.volume = 0;
					//audioSourceSystem.mute = !audioSourceSystem.mute;
					//audioSourceMusic.mute = !audioSourceMusic.mute;
				}
			} 
			else if (gameObject.name == "button_Back<") 
			{
                buttonBack2.transform.localPosition = new Vector3(-0.30f, -1.05f, 0.0f); 
				//transform.position = new Vector3 (-1, 0, 0) * Time.deltaTime;
				if (Input.GetMouseButtonDown (0))
				{
					button_Full_Audio.SetActive (false);
					button_AudioMute.SetActive (false);
					buttonBack2.SetActive (false);
					button_Graphics.SetActive (true);
					button_Audio.SetActive (true);
					buttonBack.SetActive (true);
                    title_Audio.SetActive(false);
                }
			} 
			
			//MENU GRAPHICS
			if (gameObject.name == "button_Quality_F") 
			{
                button_Quality_F.transform.localPosition = new Vector3(-0.30f, 3.06f, 0.0f);
				if (Input.GetMouseButtonDown (0))
				{
					QualitySettings.SetQualityLevel(5);
				}
			} 
			else if (gameObject.name == "button_Quality_B") 
			{
                button_Quality_B.transform.localPosition = new Vector3(-0.30f, 2.01f, 0.0f);
				//transform.position = new Vector3 (-1, 0, 0) * Time.deltaTime;
				if (Input.GetMouseButtonDown (0))    
				{
					QualitySettings.SetQualityLevel(4);
				}
			} 
			else if (gameObject.name == "button_Quality_G") 
			{
				//transform.position = new Vector3 (-1, 0, 0) * Time.deltaTime;
                button_Quality_G.transform.localPosition = new Vector3(-0.30f, 1.03f, 0.0f);
				if (Input.GetMouseButtonDown (0))
				{
					QualitySettings.SetQualityLevel(3);
				}
			} 
			else if (gameObject.name == "button_Quality_S") 
			{
				//transform.position = new Vector3 (-1, 0, 0) * Time.deltaTime;
                button_Quality_S.transform.localPosition = new Vector3(-0.30f, 0f, 0.0f);
				if (Input.GetMouseButtonDown (0))
				{
					QualitySettings.SetQualityLevel(2);
				}
			} 
			else if (gameObject.name == "button_Back") 
			{
				//transform.position = new Vector3 (-1, 0, 0) * Time.deltaTime;
                button_Back.transform.localPosition = new Vector3(-0.30f, -1.05f, 0.0f);
				if (Input.GetMouseButtonDown (0))
				{
                    menugraphics = false;
                    MenuGraphicsTurn(ref menugraphics);
                    menu = true;
                    BasicMenuTurnBool(ref menu);
                    //SceneManager.LoadScene("menu");
                    //Application.LoadLevel ("menu");
            }
			} 
			else{}

			if (gameObject.name == "buttonBack_5") 
			{
				//transform.position = new Vector3 (-1, 0, 0) * Time.deltaTime;
				if (Input.GetMouseButtonDown (0))
				{
                    menucontrols2 = false;
                    MenuControls2TurnBool(ref menucontrols2);
                    menu = true;
                    BasicMenuTurnBool(ref menu);
                    title_Controls.SetActive (false);
					//SceneManager.LoadScene("menu");
					//Application.LoadLevel ("menu");
				}
			}
             if (gameObject.name == "buttonBack_6")
             {
                if (Input.GetMouseButtonDown(0))
                {
                    menucontrols = true;
                    MenuControlsTurnBool(ref menucontrols);
                    buttonBack_6.SetActive(false);
                    buttonBack_5.SetActive(true);
                    title_Controls.GetComponent<TextMesh>().text = "Keyboard Controls :";
                }
             }
             else if (gameObject.name == "buttonNext")
             {
               if (Input.GetMouseButtonDown(0))
               {
                    menucontrols = false;
                    MenuControlsTurnBool(ref menucontrols);
                    buttonBack_5.SetActive(false);
                    buttonBack_6.SetActive(true);
                    title_Controls.GetComponent<TextMesh>().text = "Keyboard & Mouse Controls :";
               }
             }
    }

    void OnMouseExit()
    {
        //MenuStart
        if (gameObject.name == "button_Start")
        {
            button1_Start.transform.localPosition = new Vector3(0.0f, 3.34f, 0.0f);
        }
        else if (gameObject.name == "button2_Options")
        {
            button2_Options.transform.localPosition = new Vector3(0.0f, 1.9411f, 0.0f);
        }
        else if (gameObject.name == "button3_Controls")
        {
            button3_Controls.transform.localPosition = new Vector3(0.0f, 0.6599998f, 0.0f);
        }
        else if (gameObject.name == "button4_Exit")
        {
            button4_Exit.transform.localPosition = new Vector3(0.0f, -0.6100001f, 0.0f);
        }

        //MenuOptions
        if (gameObject.name == "button_Graphics")
        {
            button_Graphics.transform.localPosition = new Vector3(0.0f, 3.34f, 0.0f);
        }
        else if (gameObject.name == "button_Audio")
        {
            button_Audio.transform.localPosition = new Vector3(0.0f, 1.9411f, 0.0f);
        }
        else if (gameObject.name == "buttonBack")
        {
            buttonBack.transform.localPosition = new Vector3(0.0f, 0.5916002f, 0.0f);
        }

        //MenuGraphics
        if (gameObject.name == "button_Quality_F")
        {
            button_Quality_F.transform.localPosition = new Vector3(0f, 3.06f, 0.0f);
        }
        else if (gameObject.name == "button_Quality_B")
        {
            button_Quality_B.transform.localPosition = new Vector3(0f, 2.01f, 0.0f);
        }
        else if (gameObject.name == "button_Quality_G")
        {
            button_Quality_G.transform.localPosition = new Vector3(0f, 1.03f, 0.0f);
        }
        else if (gameObject.name == "button_Quality_S")
        {
            button_Quality_S.transform.localPosition = new Vector3(0f, 0f, 0.0f);
        }
        else if (gameObject.name == "button_Back")
        {
            button_Back.transform.localPosition = new Vector3(0f, -1.05f, 0.0f);
        }

        //MenuAudio
        if (gameObject.name == "button_Full_Audio")
        {
            button_Full_Audio.transform.localPosition = new Vector3(0f, 3.06f, 0.0f);
        }
        if (gameObject.name == "button_Audio_Mute")
        {
            button_AudioMute.transform.localPosition = new Vector3(0f, 1.94f, 0.0f);
        }
        else if (gameObject.name == "button_Back<")
        {
            buttonBack2.transform.localPosition = new Vector3(0f, -1.05f, 0.0f);
        }

    }

    void BasicMenuTurnBool(ref bool menu)
    {
        if(menu==false)
        { 
            button1_Start.SetActive(false);
            button2_Options.SetActive(false);
            button3_Controls.SetActive(false);
            button4_Exit.SetActive(false);
        }
        else if(menu==true)
        {
            button1_Start.SetActive(true);
            button2_Options.SetActive(true);
            button3_Controls.SetActive(true);
            button4_Exit.SetActive(true);
        }
    }
    void MenuControlsTurnBool(ref bool menucontrols)
    {
        if (menucontrols == false)
        {
            button1_1.SetActive(false);
            button2_2.SetActive(false);
            button3_3.SetActive(false);
            button4_4.SetActive(false);
            button5_5.SetActive(true);
            button6_6.SetActive(true);
            button7_7.SetActive(true);
            button8_8.SetActive(true);
        }
        else if (menucontrols == true)
        {
            button1_1.SetActive(true);
            button2_2.SetActive(true);
            button3_3.SetActive(true);
            button4_4.SetActive(true);
            button5_5.SetActive(false);
            button6_6.SetActive(false);
            button7_7.SetActive(false);
            button8_8.SetActive(false);
        }
    }

    void MenuControls2TurnBool(ref bool menucontrols2)
    {
        if (menucontrols2 == false)
        {
            button1_1.SetActive(false);
            button2_2.SetActive(false);
            button3_3.SetActive(false);
            button4_4.SetActive(false);
            buttonBack_5.SetActive(false);
            buttonNext.SetActive(false);
        }
        else if (menucontrols2 == true)
        {
            button1_1.SetActive(true);
            button2_2.SetActive(true);
            button3_3.SetActive(true);
            button4_4.SetActive(true);
            buttonBack_5.SetActive(true);
            buttonNext.SetActive(true);
        }
    }

    void MenuGraphicsTurn(ref bool menugraphics)
    {
        if (menugraphics == false)
        {
            title_Graphic.SetActive(false);
            button_Quality_F.SetActive(false);
            button_Quality_B.SetActive(false);
            button_Quality_G.SetActive(false);
            button_Quality_S.SetActive(false);
            button_Back.SetActive(false);
        }
        else if (menugraphics == true)
        {
            title_Graphic.SetActive(true);
            button_Quality_F.SetActive(true);
            button_Quality_B.SetActive(true);
            button_Quality_G.SetActive(true);
            button_Quality_S.SetActive(true);
            button_Back.SetActive(true);
        }
    }
   
}


